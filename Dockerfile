FROM maven:3-jdk-8-alpine as builder
EXPOSE 8080
WORKDIR /tmp
ADD pom.xml .
RUN mvn -U dependency:resolve-plugins dependency:go-offline
ADD src src
RUN ls -la
RUN mvn clean compile
RUN ls -la
RUN mvn verify -Dmaven.test.skip=true


FROM openjdk:alpine
RUN mkdir -p /app
ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for /app
RUN ln -s /app/wait-for /bin/wait-for && chmod +x /app/wait-for
RUN ls -la /bin
COPY --from=builder /tmp/target/simpleobjectstorage-0.0.1-SNAPSHOT.jar /app/simpleobjectstorage-0.0.1-SNAPSHOT.jar
WORKDIR /app
RUN ls -la
CMD ["./wait-for", "minio1:9000","-t", "300", "--", "wait-for", "mysql:3306","-t", "300", "--" ,"java", "-jar", "simpleobjectstorage-0.0.1-SNAPSHOT.jar"]