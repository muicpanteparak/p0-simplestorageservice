package teparak.me.backendtech.simpleobjectstorage.metadata;

import org.springframework.data.jpa.repository.JpaRepository;
import teparak.me.backendtech.simpleobjectstorage.entity.MetadataEntity;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface MetadataRepository extends JpaRepository<MetadataEntity, Long> {
    Optional<MetadataEntity> findByObjectAndK(ObjectModel objectModel, String k);
    List<MetadataEntity> findByObject(ObjectModel objectModel);
}
