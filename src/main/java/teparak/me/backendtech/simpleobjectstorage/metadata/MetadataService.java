package teparak.me.backendtech.simpleobjectstorage.metadata;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.entity.MetadataEntity;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectManagementService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired})
public class MetadataService {

    private final MetadataRepository metadataRepository;
    private final ObjectManagementService objectManagementService;

    public void createUpdateMetadata(BucketDao bucketDao, ObjectDao objectDao, MetadataDao metadataDao){
        ObjectModel model = objectManagementService.findCompleteObject(bucketDao, objectDao);
        Optional<MetadataEntity> metadataEntity = metadataRepository
                .findByObjectAndK(model, metadataDao.getKey());

        MetadataEntity entity;
        if (metadataEntity.isPresent()){
            entity = metadataEntity.get();
            entity.setV(metadataDao.getValue());
        } else {
            entity = MetadataEntity.of(model, metadataDao);
        }

        metadataRepository.save(entity);
    }

    public MetadataDao getMetadataByKey(BucketDao bucketDao, ObjectDao objectDao, MetadataDao metadataDao){
        return MetadataDao.of(findMetadataByKey(bucketDao, objectDao, metadataDao));
    }

    public List<MetadataDao> getAllMetadata(BucketDao bucketDao, ObjectDao objectDao){
        ObjectModel model = objectManagementService.findCompleteObject(bucketDao, objectDao);
        Stream<MetadataEntity> metadataEntity = metadataRepository
                .findByObject(model).stream();

        return metadataEntity
                .map(MetadataDao::of)
                .collect(Collectors.toList());
    }

    public void removeMetadata(BucketDao bucketDao, ObjectDao objectDao, MetadataDao metadataDao){
        MetadataEntity metadataEntity = findMetadataByKey(bucketDao, objectDao, metadataDao);
        metadataRepository.delete(metadataEntity);
    }

    private MetadataEntity  findMetadataByKey(BucketDao bucketDao, ObjectDao objectDao, MetadataDao metadataDao){
        ObjectModel model = objectManagementService.findCompleteObject(bucketDao, objectDao);
        return metadataRepository
                .findByObjectAndK(model, metadataDao.getKey())
                .orElseThrow(() -> new IllegalArgumentException("Could not find k or bucketName"));
    }
}
