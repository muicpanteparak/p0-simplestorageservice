package teparak.me.backendtech.simpleobjectstorage.metadata;

import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.entity.MetadataEntity;

@Getter
@Setter
public class MetadataDao {

    private String key;
    private String value;

    private MetadataDao(String key, String value){
        this.setValue(value);
        this.setKey(key);
    }

    private MetadataDao(String key){
        this.setKey(key);
    }

    private MetadataDao(MetadataEntity metadataEntity){
        this.setKey(metadataEntity.getK());
        this.setValue(metadataEntity.getV());
    }

    public static MetadataDao of(MetadataEntity entity){
        return new MetadataDao(entity);
    }

    public static MetadataDao of(String key, String value){
        return new MetadataDao(key, value);
    }

    public static MetadataDao of(String key){
        return new MetadataDao(key);
    }
}
