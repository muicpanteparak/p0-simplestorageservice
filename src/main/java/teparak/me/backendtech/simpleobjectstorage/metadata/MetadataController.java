package teparak.me.backendtech.simpleobjectstorage.metadata;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/{bucketName}/{objectName}")
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class MetadataController {

    private final MetadataService metadataService;

    @RequestMapping(method = RequestMethod.PUT, params = {"metadata", "key"})
    public ResponseEntity<?> createUpdateMetadata(@PathVariable String bucketName, @PathVariable String objectName, @RequestParam String key, @RequestBody String body){
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        MetadataDao metadataDao = MetadataDao.of(key, body);
        try {
            metadataService.createUpdateMetadata(bucketDao, objectDao, metadataDao);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, params = {"metadata", "key"})
    public ResponseEntity<?> deleteMetadata(@PathVariable String bucketName, @PathVariable String objectName, @RequestParam String key){
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        MetadataDao metadataDao = MetadataDao.of(key);
        try {
            metadataService.removeMetadata(bucketDao, objectDao, metadataDao);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

        }
    }

    @RequestMapping(method = RequestMethod.GET, params = {"metadata", "key"})
    public ResponseEntity<?> getMetadataByKey(@PathVariable String bucketName, @PathVariable String objectName, @RequestParam String key){
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        MetadataDao metadataDao = MetadataDao.of(key);
        try {
            MetadataDao metadata = metadataService.getMetadataByKey(bucketDao, objectDao, metadataDao);
            Map<String, String> out = new HashMap<>();
            out.put(metadata.getKey(), metadata.getValue());

            return ResponseEntity.ok(out);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, params = {"metadata"})
    public ResponseEntity<?> getAllMetadata(@PathVariable String bucketName, @PathVariable String objectName) {
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        try {
            Map<String, String> out = new HashMap<>();

            for (MetadataDao dao : metadataService.getAllMetadata(bucketDao, objectDao)) {
                out.put(dao.getKey(), dao.getValue());
            }
            return ResponseEntity.ok(out);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
