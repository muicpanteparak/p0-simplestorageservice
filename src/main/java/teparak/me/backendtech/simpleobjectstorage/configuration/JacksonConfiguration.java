package teparak.me.backendtech.simpleobjectstorage.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.*;

@Configuration
public class JacksonConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        Jackson8Module module = new Jackson8Module();
        module.addStringSerializer(LocalDateTime.class, (str) -> String.valueOf(String.valueOf(str.toEpochSecond(OffsetDateTime.now().getOffset()))));

        mapper.registerModules(module);
        return mapper;
    }

}