package teparak.me.backendtech.simpleobjectstorage.bucketmanagement;


import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketService;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;
import teparak.me.backendtech.simpleobjectstorage.exceptions.BucketNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectManagementRepository;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectManagementService;
import teparak.me.backendtech.simpleobjectstorage.model.ListObjectInBucketResponse;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired})
public class BucketManagementService {

    private final BucketManagementRepository bucketManagementRepository;
    private final ObjectManagementRepository objectManagementRepository;
//    private final ObjectManagementService objectManagementService;
    private final BucketService bucketService;

    @Transactional
    public BucketDao createBucket(BucketDao bucketDao) throws IllegalArgumentException, BucketNotFoundException{

        if (bucketDao == null || bucketDao.getBucketName() == null){
            throw new IllegalArgumentException("Bucket Name is null");
        }

        boolean bucketExist = getBucketModel(bucketDao.getBucketName()).isPresent();

        if (bucketExist){
            throw new BucketNotFoundException("Bucket Name Already Exist");
        }
        BucketModel model = bucketManagementRepository.save(BucketModel.of(bucketDao));
        // TODO: catch random k duplicate

        bucketService.createBucket(model.getRandomBucketKey());
        return BucketDao.of(model);
    }

    public void removeBucket(BucketDao bucketDao) throws IllegalArgumentException, BucketNotFoundException {


        BucketModel bucketModel = getBucketModel(bucketDao);

        String bucketKey = bucketModel.getRandomBucketKey();

        for (ObjectModel object : bucketModel.getObjectModel()){
//            objectManagementService.removeObject(BucketDao.of(bucketModel), ObjectDao.of(object));

            ObjectModel model = objectManagementRepository
                    .findByBucketModelAndObjectName(getBucketModel(bucketDao), object.getObjectName())
                    .orElseThrow(() -> new BucketNotFoundException("Bucket or Object not found"));

            for (PartObjectModel part : model.getObjectPartList()){
                bucketService.removeFile(model.getBucketModel().getRandomBucketKey(), part.getPartName());
            }
//
//            objectManagementRepository.delete(model);
        }

        bucketService.removeBucket(bucketKey);
        bucketManagementRepository.delete(bucketModel);

    }

    public BucketModel getBucketModel(BucketDao dao) throws IllegalArgumentException, BucketNotFoundException{
        if (dao == null){
            throw new IllegalArgumentException("Bucket Dao is null");
        }

        return getBucketModel(dao.getBucketName())
                .orElseThrow(() -> new BucketNotFoundException("Bucket does not exist"));
    }

    private Optional<BucketModel> getBucketModel(String bucketName) throws IllegalArgumentException{
        if (bucketName == null){
            throw new IllegalArgumentException("Bucket Name cannot be null");
        }
        return bucketManagementRepository.findByBucketName(bucketName);
    }

    public boolean isBucketExist(BucketDao bucketDao) throws IllegalArgumentException {
        if (bucketDao == null || bucketDao.getBucketName() == null){
            throw new IllegalArgumentException("Bucket Name is null");
        }

        return getBucketModel(bucketDao.getBucketName()).isPresent();

    }


    public ListObjectInBucketResponse listObjects(BucketDao bucketDao){

        BucketModel bucket = getBucketModel(bucketDao);
        Stream<ObjectModel> stream = objectManagementRepository.findByBucketModelAndCompletedTrue(bucket).stream();
        return ListObjectInBucketResponse.of(bucket, stream);
    }
}