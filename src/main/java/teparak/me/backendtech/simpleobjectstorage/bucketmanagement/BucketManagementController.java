package teparak.me.backendtech.simpleobjectstorage.bucketmanagement;


import org.springframework.http.HttpStatus;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teparak.me.backendtech.simpleobjectstorage.model.CreateBucketResponse;

@RestController
@RequestMapping("/{bucketName}")
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class BucketManagementController {

    private final BucketManagementService bucketManagementService;

    @RequestMapping(method = RequestMethod.POST, params = "create")
    public ResponseEntity<?> createBucket(@PathVariable String bucketName){
        try {
            BucketDao dao = bucketManagementService.createBucket(BucketDao.of(bucketName));

            return ResponseEntity.ok(CreateBucketResponse.of(dao));
        } catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, params = "delete")
    public ResponseEntity<?> removeBucket(@PathVariable String bucketName){
        // TODO: implement me
        try {
            bucketManagementService.removeBucket(BucketDao.of(bucketName));
            return ResponseEntity.ok().build();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, params = "list")
    public ResponseEntity<?> listObjects(@PathVariable String bucketName){
        try {
            return ResponseEntity.ok(bucketManagementService.listObjects(BucketDao.of(bucketName)));
        } catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }
}
