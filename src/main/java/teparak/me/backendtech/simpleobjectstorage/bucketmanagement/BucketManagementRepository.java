package teparak.me.backendtech.simpleobjectstorage.bucketmanagement;

import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BucketManagementRepository extends JpaRepository<BucketModel, Long> {

    Optional<BucketModel> findByBucketName(String bucketName);
}
