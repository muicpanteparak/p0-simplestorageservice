package teparak.me.backendtech.simpleobjectstorage.utils;

import com.google.common.io.BaseEncoding;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class PartObjectUtil {
    public static String getPartName(String objectName, Long partNumber){
        String partNum = String.format("%05d", partNumber);
        return String.format("%s.part%s",objectName, partNum);
    }

    public static String calculateCompleteMd5Stream(Stream<String> md5s) {
        return md5s
                .map(String::toUpperCase)
                .reduce((accu, val) -> String.join("", accu, val))
                .map(d -> BaseEncoding.base16().decode(d))
                .map(DigestUtils::md5Hex)
                .get();
    }

    public static String calculateCompleteMd5(List<String> md5s){
        return calculateCompleteMd5Stream(md5s.stream());
    }
}
