package teparak.me.backendtech.simpleobjectstorage.utils;

import com.codepoetics.protonpack.Indexed;
import com.codepoetics.protonpack.StreamUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class RangeUtils {
    private static final Pattern RANGE_EXTRACTOR = Pattern.compile("(-?\\d*)-(-?\\d*)");

    public static Indexed<Pair<Long, Long>> getFile(Stream<Pair<Long, Long>> rangePairs, Long loc){
        return StreamUtils
                .zipWithIndex(rangePairs)
                .filter(longLongPair -> isValidChunk(longLongPair.getValue().getLeft(), longLongPair.getValue().getRight(), loc))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Out of range"));
    }

    public static boolean isValidChunk(Long lo, Long hi, Long n){
        return lo <= n && hi >= n;
    }

    public static Pair<Long, Long> chunkPartition(Long requestLo, Long requestHi, Long startChunk, Long endChunk){

        if (requestLo == null){
            requestLo = startChunk;
        }

        if (requestHi == null){
            requestHi = endChunk;
        }

        if (requestLo >= requestHi){
            throw new IllegalArgumentException("Invalid range");
        }

        if  (requestHi < startChunk){
            throw new IllegalArgumentException("Invalid end range range");
        }

        if (requestLo > endChunk){
            throw new IllegalArgumentException("Invalid start range");
        }

//        if  (RangeUtils.isValidChunk(startChunk, endChunk, requestLo) && RangeUtils.isValidChunk(startChunk, endChunk, requestHi)) { //both lo and hi is within current chunk
////            return new ImmutablePair<>(requestLo, requestHi);
//
//            return new ImmutablePair<>(0L, endChunk - startChunk + 1L );
//        } else if (RangeUtils.isValidChunk(startChunk, endChunk, requestLo)){
////            return new ImmutablePair<>(requestLo - startChunk, endChunk);
//            return new ImmutablePair<>(requestHi - startChunk, endChunk - startChunk + 1L);
//
//        } else if (RangeUtils.isValidChunk(startChunk, endChunk, requestHi)){
////            return new ImmutablePair<>(0L, endChunk - startChunk); // TODO: Check correct chunk!!!
//
//            return new ImmutablePair<>(0L, requestHi - startChunk + 1L );
//
//        } else {
////            return new ImmutablePair<>(startChunk, endChunk);
//            throw new IllegalArgumentException("Broken Range");
//        }


        if  (isChunkWithin(startChunk, endChunk, requestLo) && isChunkWithin(startChunk, endChunk, requestHi)){
            System.out.println("one");
            return new ImmutablePair<>(requestLo - startChunk, requestHi - requestLo + 1);
//            return new ImmutablePair<>(requestLo - startChunk, requestLo - startChunk + (requestHi - requestLo) + 1);
        } else if (isChunkWithin(startChunk, endChunk, requestLo)){
            return new ImmutablePair<>(requestLo - startChunk, (endChunk - startChunk + 1L) - requestLo - startChunk);
        } else if (isChunkWithin(startChunk, endChunk, requestHi)){
            return new ImmutablePair<>(0L, requestHi - startChunk + 1L );
        } else {
            // entire chunk size
            return new ImmutablePair<>(0L, endChunk - startChunk + 1L );
        }
    }

    private static boolean isChunkWithin(Long start, Long end, Long n){
        return start <= n && n <= end;
    }

    public static Pair<Long, Long> rangeExtractor(String range){
        Matcher matches = RANGE_EXTRACTOR.matcher(range);
        List<Pair<Long, Long>> ranges = new ArrayList<>();

        while (matches.find()){
            Long lo;
            Long hi;

            String potentialLong;
            if (!(potentialLong = matches.group(1).trim()).isEmpty()){
                lo = parseLong(potentialLong);
            } else {
                lo = null;
            }

            if (!(potentialLong = matches.group(2).trim()).isEmpty()){
                hi = parseLong(potentialLong);
            } else {
                hi = null;
            }

            ranges.add(new ImmutablePair<>(lo, hi));
        }

        return ranges.isEmpty() ? null : ranges.get(ranges.size() - 1);
    }

    private static Long parseLong(String str){
        return Long.parseLong(str);
    }

    public static void main(String[] args) {

//        rangeExtractor("bytes=200-1000, 2000-6576, 19000-");
//        System.out.println(chunkPartition(0L, 31L, 0L, 16L)); // 0 - 17
//        System.out.println(chunkPartition(0L, 31L, 17L, 31L)); // 0 - 15
        System.out.println(chunkPartition(null, null, 0L, 16L)); // 0 - 17
        System.out.println(chunkPartition(null, null, 17L, 31L)); // 0 - 15
        System.out.println(chunkPartition(5L, 7L, 0L, 20L)); // 5 - 3

//        System.out.println(chunkPartition(1L, 18L, 0L, 16L)); // 1 - 16
//        System.out.println(chunkPartition(1L, 18L, 17L, 31L));// 0 - 2


//        System.out.println(chunkPartition(1L, 40L, 0L, 16L)); // 1 - 16
//        System.out.println(chunkPartition(1L, 40L, 17L, 31L));// 0 - 15
//        System.out.println(chunkPartition(1L, 40L, 32L, 93L));// 0 - 9

        System.out.println(chunkPartition(50L, 101L, 2L, 200L)); // 48 - 100


//        System.out.println(chunkPartition(28L, 35L, 10L, 25L));
    }
}
