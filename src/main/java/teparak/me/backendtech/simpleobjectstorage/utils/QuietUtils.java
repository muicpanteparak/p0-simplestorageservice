package teparak.me.backendtech.simpleobjectstorage.utils;

import org.apache.commons.io.IOUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class QuietUtils {
    public static boolean closeable(Closeable closeable){
        try {
            if (closeable != null){
                closeable.close();
            }
        } catch (IOException e) {
            return false;
        }
        return true;

    }
}
