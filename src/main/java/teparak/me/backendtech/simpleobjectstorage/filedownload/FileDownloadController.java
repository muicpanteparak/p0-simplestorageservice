package teparak.me.backendtech.simpleobjectstorage.filedownload;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectManagementService;
import teparak.me.backendtech.simpleobjectstorage.utils.RangeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@RestController
@RequestMapping("/{bucketName}/{objectName}")
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class FileDownloadController {

    private final FileDownload fileDownload;
    private final ObjectManagementService objectManagementService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> downloadEntireFile(@PathVariable String bucketName, @PathVariable String objectName, HttpServletResponse response) throws IOException {
        OutputStream out = response.getOutputStream();

        fileDownload.requestDownload(BucketDao.of(bucketName), ObjectDao.of(objectName), out);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET, headers = "Range")
    public ResponseEntity<?> downloadRangeFile(
            @PathVariable String bucketName,
            @PathVariable String objectName,
            @RequestHeader String range,
            HttpServletResponse response) throws IOException {

        Pair<Long, Long> requestRange = RangeUtils.rangeExtractor(range);
        BucketDao bucket = BucketDao.of(bucketName);
        ObjectDao object = ObjectDao.of(objectName);
        OutputStream outputStream = response.getOutputStream();


        fileDownload.requestRangeDownload(bucket, object, outputStream, requestRange.getLeft(), requestRange.getRight());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.DELETE, params = "delete")
    public ResponseEntity<?> deleteObject(@PathVariable String bucketName, @PathVariable String objectName) {

        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        try {
            objectManagementService.removeObject(bucketDao, objectDao);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return null;
    }

}
