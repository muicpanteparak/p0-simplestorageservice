package teparak.me.backendtech.simpleobjectstorage.filedownload;

import com.codepoetics.protonpack.Indexed;
import io.minio.messages.Bucket;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketService;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectManagementService;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.utils.QuietUtils;
import teparak.me.backendtech.simpleobjectstorage.utils.RangeUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class FileDownload {

    private final BucketService bucketService;
    private final ObjectManagementService objectManagementService;
    private int BUFFER_SIZE = 1024 * 16;

    public void requestDownload(BucketDao bucketDao, ObjectDao objectDao, OutputStream out){
        // Let the caller close socket!

        ObjectModel model = objectManagementService.findCompleteObject(bucketDao, objectDao);

        String bucketKey = model.getBucketModel().getRandomBucketKey();
        AtomicReference<Long> accu = new AtomicReference<>((long) 0);
        Stream<Triple<String, Long, Long>> parts = model.getObjectPartList()
                .stream()
                .map(itm -> {
                    String name = itm.getPartName();
                    long size = itm.getPartLength();
                    accu.updateAndGet(v -> v + size);
                    return new ImmutableTriple<>(name, size, accu.get());
                });

        try {
            for (Iterator<Triple<String, Long, Long>> tmp = parts.iterator(); tmp.hasNext(); ){
                String partName = tmp.next().getLeft();
//                Long length = tmp.getMiddle();
//                Long accumulate = tmp.getRight();

                InputStream in = bucketService.getObject(bucketKey, partName);
                IOUtils.copy(in, out, BUFFER_SIZE);
                in.close();
            }
        } catch (IOException e){

        }
    }

    public void requestRangeDownload(@NonNull BucketDao bucketDao, @NonNull ObjectDao objectDao, @NonNull OutputStream out, Long lo, Long hi){
        Stream<Pair<Long, Long>> partsLo = objectManagementService
                .findCompleteObject(bucketDao, objectDao)
                .getObjectPartList()
                .stream()
                .map(v -> new ImmutablePair<>(v.getStartChunk(), v.getEndChunk()));

        Stream<Pair<Long, Long>> partsHi = objectManagementService
                .findCompleteObject(bucketDao, objectDao)
                .getObjectPartList()
                .stream()
                .map(v -> new ImmutablePair<>(v.getStartChunk(), v.getEndChunk()));


        System.out.println(partsLo);
        Indexed<Pair<Long, Long>> begin = RangeUtils.getFile(partsLo, lo);
        Indexed<Pair<Long, Long>> end = RangeUtils.getFile(partsHi, hi);

        ObjectModel model = objectManagementService.findCompleteObject(bucketDao, objectDao);
        String bucketKey = model.getBucketModel().getRandomBucketKey();

        model
                .getObjectPartList()
                .stream()
                .skip(begin.getIndex())
                .limit(end.getIndex() - begin.getIndex() + 1)
                .forEach((chunk) -> {
                    String objectName = chunk.getPartName();

                    InputStream in;
                    Pair<Long, Long> partition = RangeUtils.chunkPartition(lo, hi, chunk.getStartChunk(), chunk.getEndChunk());
                    System.out.printf("PartName: %s, Chunk Start: %d, Chunk End: %d\n", chunk.getPartName(), partition.getLeft(), partition.getRight());


                    // Minio Client has bug. See https://github.com/minio/minio-java/issues/706
                    if (partition.getLeft() == 0){
                        in = bucketService.getObject(bucketKey, objectName);
                    } else {
                        in = bucketService.getObject(bucketKey, objectName, partition.getLeft(), partition.getRight());
                    }

                    try {

                        if (partition.getLeft() == 0){
                            IOUtils.copyLarge(in, out, 0, partition.getRight());
                             QuietUtils.closeable(in);
                        } else {
                            IOUtils.copy(in, out, BUFFER_SIZE);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        QuietUtils.closeable(in);
                    }
                });
    }
}
