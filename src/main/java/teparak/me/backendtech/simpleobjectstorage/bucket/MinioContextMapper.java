package teparak.me.backendtech.simpleobjectstorage.bucket;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties("bucket")
public class MinioContextMapper {

    private String endpoint;
    private String accessKey;
    private String secretKey;
}
