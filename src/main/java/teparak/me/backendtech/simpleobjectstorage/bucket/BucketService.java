package teparak.me.backendtech.simpleobjectstorage.bucket;

import lombok.NonNull;
import org.xmlpull.v1.XmlPullParserException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.ObjectNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.utils.PartObjectUtil;
import teparak.me.backendtech.simpleobjectstorage.exceptions.BucketCommunicationException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.BucketCreationException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.PutObjectException;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
//@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class BucketService {

//    TODO: initialise minioClient inside @PostConstruct, throw exception if failed to communicate!

    private MinioClient minioClient;

    @Autowired
    public BucketService(@NonNull MinioContextMapper minioContextMapper) throws BucketCommunicationException {
        try {
            this.minioClient = new MinioClient(minioContextMapper.getEndpoint(),minioContextMapper.getAccessKey(), minioContextMapper.getSecretKey());
        } catch (Exception e) {
            throw new BucketCommunicationException(e);
        }
    }

    public void removeBucket(@NonNull String bucketName){
        // Check if bucket exist
        try {
            minioClient.removeBucket(bucketName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createBucket(@NonNull String bucketName) throws BucketCreationException{
        // Check if bucket exist
        try {
            minioClient.makeBucket(bucketName);
        } catch (Exception e){
            throw new BucketCreationException(e);
        }
    }

    public void putObject(@NonNull String bucketName, @NonNull String fileName, @Nonnull Long partNumber, @NonNull InputStream in) throws PutObjectException{

        // Check if bucket exist
        try {
            minioClient.putObject(bucketName, PartObjectUtil.getPartName(fileName, partNumber), in, null);

        } catch (Exception e) {
            throw new PutObjectException(e);
        }
    }

    public void removeFile(@NonNull String bucketName, @NonNull String fileKey) {
        // Check if file exist
        try {
            minioClient.removeObject(bucketName, fileKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void moveFile(@NonNull String bucketName, @Nonnull String fileKey, String newFileKey){
        try {
            minioClient.copyObject(bucketName, fileKey, bucketName, newFileKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public InputStream getObject(@NonNull String bucketName, @NonNull String fileName) {
        try {
            return minioClient.getObject(bucketName, fileName);
        } catch (Exception e) {
            throw new ObjectNotFoundException(e);
        }
    }

    public InputStream getObject(@NonNull String bucketName, @NonNull String fileName, @NonNull Long offset, Long length) {
        try {
            return minioClient.getObject(bucketName, fileName, offset, length);
        } catch (Exception e){
            throw new ObjectNotFoundException(e);
        }
    }
}
