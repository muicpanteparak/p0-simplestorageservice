package teparak.me.backendtech.simpleobjectstorage.bucket;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;

import java.time.LocalDateTime;

@Getter
@Setter
public class BucketDao {

    @JsonIgnore
    private Long bucketId;
    private String bucketName;
    private String randomBucketKey;
    private LocalDateTime created;
    private LocalDateTime modified;

    private BucketDao(String bucketName) {
        this.bucketName = bucketName;
    }

    private BucketDao(BucketModel model){
        this.bucketId = model.getId();
        this.bucketName = model.getBucketName();
        this.created = model.getCreatedDate();
        this.modified = model.getModifiedDate();
        this.randomBucketKey = model.getRandomBucketKey();
    }

    public static BucketDao of(BucketModel bucketModel){
        return new BucketDao(bucketModel);
    }

    public static BucketDao of(String bucketName){
        return new BucketDao(bucketName);
    }

}
