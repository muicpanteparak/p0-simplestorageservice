package teparak.me.backendtech.simpleobjectstorage.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;
import teparak.me.backendtech.simpleobjectstorage.utils.QuietUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Getter
@Setter
public class ListObjectInBucketResponse {
    private LocalDateTime created;
    private LocalDateTime modified;
    private String name;
    private List<FileObject> objects;

    private ListObjectInBucketResponse(BucketDao bucketDao, List<ObjectDao> objectDaoList) {
        this.setCreated(bucketDao.getCreated());
        this.setModified(bucketDao.getModified());
        this.setName(bucketDao.getBucketName());
        this.objects = objectDaoList
                .stream()
                .map(FileObject::new)
                .collect(Collectors.toList());
    }

    public static ListObjectInBucketResponse of(BucketModel model, Stream<ObjectModel> modelStream){
        Stream<ObjectDao> out = modelStream
                .map(ObjectDao::of);

        return of(BucketDao.of(model), out);

    }

    public static ListObjectInBucketResponse of(BucketDao bucketDao, Stream<ObjectDao> objectDaoStream){
        List<ObjectDao> objectDaos = objectDaoStream.collect(Collectors.toList());
        objectDaoStream.close();
        return new ListObjectInBucketResponse(bucketDao, objectDaos);
    }

    @Getter
    @Setter
    private static class FileObject {
        private String name;
        private String eTag;
        private LocalDateTime created;
        private LocalDateTime modified;


        private FileObject(ObjectDao objectDao) {
            this.setName(objectDao.getObjectName());
            this.setETag(objectDao.getETag());
            this.setCreated(objectDao.getCreated());
            this.setModified(objectDao.getModified());
        }

        public static FileObject of(ObjectDao dao){
            return new FileObject(dao);
        }
    }
}
