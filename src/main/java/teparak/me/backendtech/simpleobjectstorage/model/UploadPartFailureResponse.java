package teparak.me.backendtech.simpleobjectstorage.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadPartFailureResponse {
    private Long length;
    private Long partNumber;
    private PartResponseError error;

    private enum PartResponseError {
        LengthMismatched,
        MD5Mismatched,
        InvalidPartNumber,
        InvalidObjectName,
        InvalidBucket
    }
}
