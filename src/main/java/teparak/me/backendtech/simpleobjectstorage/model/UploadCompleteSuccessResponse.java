package teparak.me.backendtech.simpleobjectstorage.model;

public class UploadCompleteSuccessResponse {
    private String eTag;
    private Long length;
    private String name;
    private String error;

    private static enum ErrorResponse {
        InvalidObjectName, InvalidBucket;
    }
}
