package teparak.me.backendtech.simpleobjectstorage.model;

import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;

import java.time.LocalDateTime;

@Getter
@Setter
public class CreateBucketResponse {

    private LocalDateTime created;
    private LocalDateTime modified;
    private String name;

    private CreateBucketResponse(BucketDao bucketDao){
        this.created = bucketDao.getCreated();
        this.modified = bucketDao.getModified();
        this.name = bucketDao.getBucketName();
    }

    public static CreateBucketResponse of(BucketDao bucketDao){
        return new CreateBucketResponse(bucketDao);
    }
}
