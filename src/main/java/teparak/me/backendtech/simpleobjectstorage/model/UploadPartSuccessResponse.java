package teparak.me.backendtech.simpleobjectstorage.model;

import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;
import teparak.me.backendtech.simpleobjectstorage.fileupload.PartObjectDao;

@Setter
@Getter
public class UploadPartSuccessResponse {
    private String md5;
    private Long length;
    private Long partNumber;

    private UploadPartSuccessResponse(PartObjectDao partObjectDao){
        this.setMd5(partObjectDao.getMd5());
        this.setLength(partObjectDao.getPartLength());
        this.setPartNumber(partObjectDao.getPartNumber());
    }

    public static UploadPartSuccessResponse of(PartObjectDao dao){
        return new UploadPartSuccessResponse(dao);
    }

    public static UploadPartSuccessResponse of(PartObjectModel model){
        return of(PartObjectDao.of(model));
    }
}
