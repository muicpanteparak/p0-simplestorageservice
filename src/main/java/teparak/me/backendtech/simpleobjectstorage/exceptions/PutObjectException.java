package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class PutObjectException extends RuntimeException {

    public PutObjectException() {
        super();
    }

    public PutObjectException(String message) {
        super(message);
    }

    public PutObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public PutObjectException(Throwable cause) {
        super(cause);
    }

    protected PutObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
