package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class PartObjectException extends RuntimeException {
    public PartObjectException() {
        super();
    }

    public PartObjectException(String message) {
        super(message);
    }

    public PartObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public PartObjectException(Throwable cause) {
        super(cause);
    }

    protected PartObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
