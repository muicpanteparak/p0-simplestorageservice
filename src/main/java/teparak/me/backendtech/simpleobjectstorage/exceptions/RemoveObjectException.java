package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class RemoveObjectException extends RuntimeException {
    public RemoveObjectException() {
        super();
    }

    public RemoveObjectException(String message) {
        super(message);
    }

    public RemoveObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public RemoveObjectException(Throwable cause) {
        super(cause);
    }

    protected RemoveObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
