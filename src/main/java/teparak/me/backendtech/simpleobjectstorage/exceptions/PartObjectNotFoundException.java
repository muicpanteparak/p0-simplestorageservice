package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class PartObjectNotFoundException extends RuntimeException {
    public PartObjectNotFoundException() {
        super();
    }

    public PartObjectNotFoundException(String message) {
        super(message);
    }

    public PartObjectNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PartObjectNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PartObjectNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
