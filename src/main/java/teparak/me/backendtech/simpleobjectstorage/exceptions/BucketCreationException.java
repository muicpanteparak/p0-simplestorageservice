package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class BucketCreationException extends RuntimeException {
    public BucketCreationException() {
        super();
    }

    public BucketCreationException(String message) {
        super(message);
    }

    public BucketCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketCreationException(Throwable cause) {
        super(cause);
    }

    protected BucketCreationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
