package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class BucketCommunicationException extends RuntimeException {
    public BucketCommunicationException() {

    }

    public BucketCommunicationException(String message) {
        super(message);
    }

    public BucketCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketCommunicationException(Throwable cause) {
        super(cause);
    }

    protected BucketCommunicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
