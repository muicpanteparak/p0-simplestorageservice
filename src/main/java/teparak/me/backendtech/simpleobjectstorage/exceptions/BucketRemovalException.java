package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class BucketRemovalException extends RuntimeException {
    public BucketRemovalException() {
        super();
    }

    public BucketRemovalException(String message) {
        super(message);
    }

    public BucketRemovalException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketRemovalException(Throwable cause) {
        super(cause);
    }

    protected BucketRemovalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
