package teparak.me.backendtech.simpleobjectstorage.exceptions;

public class MD5MismatchException extends RuntimeException {
    public MD5MismatchException() {
        super();
    }

    public MD5MismatchException(String message) {
        super(message);
    }

    public MD5MismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public MD5MismatchException(Throwable cause) {
        super(cause);
    }

    protected MD5MismatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
