package teparak.me.backendtech.simpleobjectstorage.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.metadata.MetadataDao;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MetadataEntity extends AbstractEntity {

    @ManyToOne
    private ObjectModel object;
    private String k;
    private String v;

    private MetadataEntity(ObjectModel model, MetadataDao metadataDao){
        this.setK(metadataDao.getKey());
        this.setV(metadataDao.getValue());
        this.setObject(model);
    }

    public static MetadataEntity of(ObjectModel model, MetadataDao metadataDao){
        return new MetadataEntity(model, metadataDao);
    }
}
