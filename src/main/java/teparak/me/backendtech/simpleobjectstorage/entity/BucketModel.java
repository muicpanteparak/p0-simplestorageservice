package teparak.me.backendtech.simpleobjectstorage.entity;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Map;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class BucketModel extends AbstractEntity {

    private String bucketName;
    private String randomBucketKey = RandomStringUtils.randomAlphanumeric(3, 64).toLowerCase();

    @OneToMany(mappedBy = "bucketModel", cascade = CascadeType.ALL)
    private List<ObjectModel> objectModel;


    private BucketModel(BucketDao dao) {
        this.bucketName = dao.getBucketName();
    }

    public static BucketModel of(BucketDao dao){
        return new BucketModel(dao);
    }
}
