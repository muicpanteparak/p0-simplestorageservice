package teparak.me.backendtech.simpleobjectstorage.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.utils.PartObjectUtil;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PartObjectModel extends AbstractEntity implements Comparable<PartObjectModel> {

    @ManyToOne
    private ObjectModel objectModel;

    private Long partNumber;

    @Transient
    private String partName;

    private Long partLength;
    private Boolean completed;
    private String md5Sum;
    private Long startChunk;
    private Long endChunk;

    @Override
    public int compareTo(PartObjectModel o) {
        return this.getPartLength().compareTo(o.getPartNumber());
    }

    private PartObjectModel(Long partNumber, ObjectModel objectModel){
        this.partNumber = partNumber;
        this.objectModel = objectModel;
        this.completed = false;
        this.partName = PartObjectUtil.getPartName(objectModel.getObjectName(), partNumber);
    }

    public static PartObjectModel of(ObjectModel objectModel, Long partNumber){
        return new PartObjectModel(partNumber, objectModel);
    }

    public String getPartName() {
        if (partName == null){
            this.setPartName(PartObjectUtil.getPartName(objectModel.getObjectName(), partNumber));
        }
        return partName;
    }
}
