package teparak.me.backendtech.simpleobjectstorage.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime createdDate;

    @Column(nullable = false)
    private LocalDateTime modifiedDate;


    @PrePersist
    public void prePersist(){
        LocalDateTime time = LocalDateTime.now();
        createdDate = time;
        modifiedDate = time;
    }

    @PreUpdate
    public void preUpdate() {
        LocalDateTime time = LocalDateTime.now();
        modifiedDate = time;
    }
}
