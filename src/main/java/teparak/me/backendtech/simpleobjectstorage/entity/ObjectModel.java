package teparak.me.backendtech.simpleobjectstorage.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import teparak.me.backendtech.simpleobjectstorage.fileupload.ObjectDao;

import javax.persistence.*;
import java.util.List;

@Getter
@Entity
@Setter
@NoArgsConstructor
public class ObjectModel extends AbstractEntity {

    @Column(nullable = false)
    private String objectName;

    @ColumnDefault("false")
    @Column(nullable = false)
    private Boolean completed;

    @OrderBy("partNumber")
    @OneToMany(mappedBy = "objectModel", cascade = CascadeType.ALL)
    private List<PartObjectModel> objectPartList;

    @OneToMany(mappedBy = "object", cascade = CascadeType.ALL)
    private List<MetadataEntity> metadataEntities;

    @ManyToOne
    private BucketModel bucketModel;

    private String eTag;
    private Long contentLength;

    private ObjectModel(ObjectDao objectDao){
        this.setObjectName(objectDao.getObjectName());
        this.setCompleted(objectDao.getComplete());
        this.setId(objectDao.getObjectId());
    }

    public static ObjectModel of(ObjectDao objectDao){
        return new ObjectModel(objectDao);
    }
}
