package teparak.me.backendtech.simpleobjectstorage.fileupload;

import org.springframework.data.jpa.repository.JpaRepository;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface ObjectManagementRepository extends JpaRepository<ObjectModel, Long> {
    Optional<ObjectModel> findByBucketModelAndObjectNameAndCompletedTrue(BucketModel bucketModel, String objectName);
    Optional<ObjectModel> findByIdAndCompletedFalse(Long id);
    Optional<ObjectModel> findByBucketModelAndObjectNameAndCompletedFalse(BucketModel bucketModel, String objectName);
    List<ObjectModel> findByBucketModelAndCompletedTrue(BucketModel bucketModel);
    Optional<ObjectModel> findByBucketModelAndObjectName(BucketModel bucketModel, String objectName);
}
