package teparak.me.backendtech.simpleobjectstorage.fileupload;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketService;
import teparak.me.backendtech.simpleobjectstorage.bucketmanagement.BucketManagementService;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;
import teparak.me.backendtech.simpleobjectstorage.exceptions.BucketNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.DuplicateObjectException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.ObjectNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.TicketNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.utils.PartObjectUtil;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired})
public class ObjectManagementService {

    private final ObjectManagementRepository objectManagementRepository;
    private final BucketManagementService bucketManagementService;
    private final BucketService bucketService;

    public ObjectDao createNewObject(BucketDao bucketDao, ObjectDao objectDao) throws DuplicateObjectException {

        // Check if object already exist

        if (findCompleteObject(bucketManagementService.getBucketModel(bucketDao), objectDao).isPresent()){
            throw new DuplicateObjectException("Object already exist");
        }


        BucketModel bucketModel = bucketManagementService.getBucketModel(bucketDao);
        ObjectModel objectModel = ObjectModel.of(objectDao);
        objectModel.setBucketModel(bucketModel);
        objectModel.setCompleted(false);
        objectModel = objectManagementRepository.save(objectModel);

        return ObjectDao.of(objectModel);
    }

    private Optional<ObjectModel> findCompleteObject(BucketModel bucketModel, ObjectDao objectDao){
        if (objectDao.getObjectId() != null){
            return objectManagementRepository.findById(objectDao.getObjectId());
        }

        return objectManagementRepository
                .findByBucketModelAndObjectNameAndCompletedTrue(bucketModel, objectDao.getObjectName());
    }

    public ObjectModel findCompleteObject(BucketDao bucketDao, ObjectDao objectDao) throws BucketNotFoundException {

        return findCompleteObject(bucketManagementService.getBucketModel(bucketDao), objectDao)
                .orElseThrow(() -> new BucketNotFoundException(String.format("/%s/%s does not exist", bucketDao.getBucketName(), objectDao.getObjectName())));
    }

    private Optional<ObjectModel> findIncompleteObject(BucketModel bucketModel, ObjectDao objectDao){
        if (objectDao.getObjectId() != null){
            return objectManagementRepository.findById(objectDao.getObjectId());
        }

        return objectManagementRepository
                .findByBucketModelAndObjectNameAndCompletedFalse(bucketModel, objectDao.getObjectName());
    }

    public ObjectModel findIncompleteObject(BucketDao bucketDao, ObjectDao objectDao) throws ObjectNotFoundException {
        return findIncompleteObject(bucketManagementService.getBucketModel(bucketDao), objectDao)
                .orElseThrow(() -> new ObjectNotFoundException(String.format("/%s/%s does not exist (incomplete object)", bucketDao.getBucketName(), objectDao.getObjectName())));
    }

    public ObjectDao completeTicket(BucketDao bucketDao, ObjectDao objectDao) throws NoSuchElementException  {


        Optional<ObjectModel> objectModelIsPresent = findIncompleteObject(bucketManagementService.getBucketModel(bucketDao), objectDao);
        ObjectModel objectModel;

        if (objectModelIsPresent.isPresent()){
            objectModel = objectModelIsPresent.get();
        } else {
            throw new ObjectNotFoundException("Object Not Found");
        }


        AtomicReference<Long> atomicReference = new AtomicReference<>((long) 0);
        objectModel.setCompleted(true);

        // assume sorted by database
        objectModel
                .getObjectPartList()
                .stream()
                .forEach(model -> {
                    if (atomicReference.get() == 0){
                        model.setStartChunk(0L);
                    } else {
                        model.setStartChunk(atomicReference.updateAndGet(x -> x + 1));
                    }
                    model.setEndChunk(atomicReference.updateAndGet(x -> x + model.getPartLength() - 1));
                });

        Stream<String> md5s = objectModel
                .getObjectPartList()
                .stream()
                .map(PartObjectModel::getMd5Sum);

        objectModel.setETag(String.join("-", PartObjectUtil.calculateCompleteMd5Stream(md5s), String.valueOf(objectModel.getObjectPartList().size())));
        objectModel.setContentLength(atomicReference.get() + 2);

        ObjectDao ret = ObjectDao.of(objectManagementRepository.save(objectModel));
        return ret;
    }

//    public ObjectDao removeIncompleteObjectPart(BucketDao bucketDao, ObjectDao objectDao, Long partNumber){
////        BucketModel bucket = bucketManagementService.getBucketModel(bucketDao);
//        partObjectManagementService.removePartObject(bucketDao, ObjectDao.of(objectModel), partNumber);
//
//
//        return null;
//    }

    public void removeObject(BucketDao bucketDao, ObjectDao objectDao) throws BucketNotFoundException {
        ObjectModel objectModel = findCompleteObject(bucketDao, objectDao);
        objectModel.getObjectPartList()
                .stream()
                .forEach(object -> {
                    bucketService.removeFile(objectModel.getBucketModel().getRandomBucketKey(), object.getPartName());
                });
        objectManagementRepository.delete(objectModel);
    }
}
