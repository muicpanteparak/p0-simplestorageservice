package teparak.me.backendtech.simpleobjectstorage.fileupload;

import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;

import java.time.LocalDateTime;

@Getter
@Setter
public class ObjectDao {
    private Long objectId;
    private String objectName;
    private Boolean complete;
    private String eTag;
    private LocalDateTime created;
    private LocalDateTime modified;
    private Long contentLength;

    private ObjectDao(ObjectModel objectModel){
        this.setObjectName(objectModel.getObjectName());
        this.setComplete(objectModel.getCompleted());
        this.setObjectId(objectModel.getId());
        this.setETag(objectModel.getETag());
        this.setCreated(objectModel.getCreatedDate());
        this.setModified(objectModel.getModifiedDate());
        this.setContentLength(objectModel.getContentLength());
    }

    private ObjectDao(String objectName){
        this.objectName = objectName;
    }

    public static ObjectDao of(ObjectModel objectModel){
        return new ObjectDao(objectModel);
    }

    public static ObjectDao of(String fileName) {
        return new ObjectDao(fileName);
    }
}
