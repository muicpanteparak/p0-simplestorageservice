package teparak.me.backendtech.simpleobjectstorage.fileupload;

import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketService;
import teparak.me.backendtech.simpleobjectstorage.bucketmanagement.BucketManagementService;
import teparak.me.backendtech.simpleobjectstorage.entity.BucketModel;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.exceptions.MD5MismatchException;
import teparak.me.backendtech.simpleobjectstorage.utils.PartObjectUtil;

import javax.servlet.ServletInputStream;
import javax.transaction.Transactional;
import java.io.*;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class FileUploadService {

//    private int STREAM_BUFFER_SIZE = 8192;
    private final BucketManagementService bucketManagementService;
    private final ObjectManagementService objectManagementService;
    private final PartObjectManagementService partObjectManagementService;
    private final BucketService bucketService;

    @Transactional
    public void createTicket(BucketDao bucketDao, ObjectDao objectDao){
        // TODO: validate if ticket for object exist regardless of completion
        objectManagementService.createNewObject(bucketDao, objectDao);
    }

    @Transactional
    public void uploadPartToBucket(BucketDao bucketDao, ObjectDao objectDao, Long partNumber, ServletInputStream in, Long contentLength, String md5) throws IOException {

//        TODO: ERR: It is only valid to switch to non-blocking IO within async processing or HTTP upgrade processing
//        in.setReadListener(new ReadListener() {
//            @Override
//            public void onDataAvailable() throws IOException {
//                partObjectManagementService.addOrUpdatePartObject(bucketDao, objectDao, partNumber);
//                bucketService.putObject(bucketDao.getBucketName(), objectDao.getPartName(), partNumber, in);
//            }
//
//            @Override
//            public void onAllDataRead() throws IOException {
//                partObjectManagementService.completedPartObject(objectDao, partNumber);
//                in.close();
//            }
//
//            @Override
//            public void onError(Throwable throwable) {
//                // Clean up etc error handling
//            }
//        });

//        BucketModel bucketModel = bucketManagementService.getBucketModel(bucketDao);
//        // TODO: Compare MD5
//
//        ObjectModel objectModel = objectManagementService.findIncompleteObject(bucketDao, objectDao);
//        bucketDao = BucketDao.of(bucketModel);
//        objectDao = ObjectDao.of(objectModel);
//
//        PartObjectDao partObjectDao = partObjectManagementService.addOrUpdatePartObject(bucketDao, objectDao, partNumber, contentLength);
//        bucketService.putObject(bucketDao.getRandomBucketKey(), objectDao.getObjectName(), partNumber, in);
//        in.close();
//
//        InputStream inStream = bucketService.getObject(bucketDao.getRandomBucketKey(), partObjectDao.getPartName());
//        String md5Val = DigestUtils.md5Hex(inStream);
//
//        partObjectManagementService.completedPartObject(objectDao, partNumber, md5Val);




        //

        BucketModel bucketModel = bucketManagementService.getBucketModel(bucketDao);
        ObjectModel objectModel = objectManagementService.findIncompleteObject(bucketDao, objectDao);
        bucketDao = BucketDao.of(bucketModel);
        objectDao = ObjectDao.of(objectModel);
        bucketService.putObject(bucketDao.getRandomBucketKey(), objectDao.getObjectName() + ".incomplete", partNumber, in);

        InputStream inStream = bucketService.getObject(bucketDao.getRandomBucketKey(), PartObjectUtil.getPartName(objectDao.getObjectName() + ".incomplete", partNumber));
        String md5Val = DigestUtils.md5Hex(inStream);

        if (!md5.equals(md5Val)){
            bucketService.removeFile(bucketDao.getRandomBucketKey(), PartObjectUtil.getPartName(objectDao.getObjectName() + ".incomplete", partNumber));
            throw new MD5MismatchException("MD5 Mismatch");
        }


        bucketService.moveFile(bucketDao.getRandomBucketKey(), PartObjectUtil.getPartName(objectDao.getObjectName() + ".incomplete", partNumber), PartObjectUtil.getPartName(objectDao.getObjectName(), partNumber));
        bucketService.removeFile(bucketDao.getRandomBucketKey(), PartObjectUtil.getPartName(objectDao.getObjectName() + ".incomplete", partNumber));
        in.close();
        partObjectManagementService.addOrUpdatePartObject(bucketDao, objectDao, partNumber, contentLength);
        partObjectManagementService.completedPartObject(objectDao, partNumber, md5Val);
    }

    @Transactional
    public ObjectDao completeTicket(BucketDao bucketDao, ObjectDao objectDao) throws NoSuchElementException {
       return objectManagementService.completeTicket(bucketDao, objectDao);
    }

    @Transactional
    public void deleteIncompletePart(BucketDao bucketDao, ObjectDao objectDao, Long partNumber){
//        objectManagementService.removeIncompleteObjectPart(bucketDao, objectDao, partNumber);
        partObjectManagementService.removePartObject(bucketDao, objectDao, partNumber);
    }
}
