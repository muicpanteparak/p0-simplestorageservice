package teparak.me.backendtech.simpleobjectstorage.fileupload;

import org.springframework.data.jpa.repository.JpaRepository;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;

import java.util.Optional;

public interface PartObjectManagementRepository extends JpaRepository<PartObjectModel, Long> {
    Optional<PartObjectModel> findByObjectModelAndPartNumber(ObjectModel objectModel, Long partNumber);
}
