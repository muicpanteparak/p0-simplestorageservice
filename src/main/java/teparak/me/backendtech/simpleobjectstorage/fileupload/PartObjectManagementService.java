package teparak.me.backendtech.simpleobjectstorage.fileupload;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketService;
import teparak.me.backendtech.simpleobjectstorage.exceptions.PartObjectException;
import teparak.me.backendtech.simpleobjectstorage.entity.ObjectModel;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;
import teparak.me.backendtech.simpleobjectstorage.exceptions.PartObjectNotFoundException;

import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_={ @Autowired})
public class PartObjectManagementService {

    private final PartObjectManagementRepository partObjectManagementRepository;
    private final ObjectManagementService objectManagementService;
    private final BucketService bucketService;

    public PartObjectDao addOrUpdatePartObject(BucketDao bucketDao, ObjectDao objectDao, Long partNumber, Long contentLength){
        // TODO Validate part range

        ObjectModel objectModel =  objectManagementService.findIncompleteObject(bucketDao, objectDao);

        Optional<PartObjectModel> partObject = partObjectManagementRepository.findByObjectModelAndPartNumber(objectModel, partNumber);
        PartObjectModel  partObjectModel = partObject
                .orElseGet(() -> PartObjectModel.of(objectModel, partNumber));
        partObjectModel.setPartLength(contentLength);
        partObjectModel = partObjectManagementRepository.save(partObjectModel);
        return PartObjectDao.of(partObjectModel);
    }

    public void removePartObject(BucketDao bucketDao, ObjectDao objectDao, Long partNumber) {

        ObjectModel objectModel = objectManagementService.findIncompleteObject(bucketDao, objectDao);
        PartObjectModel partObjectModel = partObjectManagementRepository
                .findByObjectModelAndPartNumber(objectModel, partNumber)
                .orElseThrow(() -> new PartObjectNotFoundException("Object not found"));

        String bucketKey = bucketDao.getRandomBucketKey();
        String fileKey = partObjectModel.getPartName();


        PartObjectModel partObject = partObjectManagementRepository.findByObjectModelAndPartNumber(objectModel, partObjectModel.getPartNumber()).orElseThrow(() -> new PartObjectNotFoundException("Part not found"));
        partObjectManagementRepository.delete(partObject);
        bucketService.removeFile(bucketKey, fileKey);
    }

    public void completedPartObject(ObjectDao objectDao, Long partNumber, String md5sum){

        ObjectModel objectModel = ObjectModel.of(objectDao);
        PartObjectModel partObjectModel = partObjectManagementRepository
                .findByObjectModelAndPartNumber(objectModel, partNumber)
                .orElseThrow(() -> new PartObjectException("Part Does not exist"));

        partObjectModel.setCompleted(true);
        partObjectModel.setMd5Sum(md5sum);
        partObjectManagementRepository.save(partObjectModel);
    }
}
