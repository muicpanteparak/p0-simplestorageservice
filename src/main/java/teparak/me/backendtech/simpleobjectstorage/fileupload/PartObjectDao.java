package teparak.me.backendtech.simpleobjectstorage.fileupload;


import lombok.Getter;
import lombok.Setter;
import teparak.me.backendtech.simpleobjectstorage.entity.PartObjectModel;

@Getter
@Setter
public class PartObjectDao {

    private Long id;
    private ObjectDao objectModel;
    private Long partNumber;
    private Long partLength;
    private Boolean completed;
    private String partName;
    private String md5;

    private PartObjectDao(PartObjectModel model){
        this.setId(model.getId());
        this.objectModel = ObjectDao.of(model.getObjectModel());
        this.partNumber = model.getPartNumber();
        this.partLength = model.getPartLength();
        this.completed = model.getCompleted();
        this.partName = model.getPartName();
        this.setMd5(model.getMd5Sum());
    }

    public static PartObjectDao of(PartObjectModel model){
        return new PartObjectDao(model);
    }
}
