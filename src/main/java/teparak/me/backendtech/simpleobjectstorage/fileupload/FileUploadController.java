package teparak.me.backendtech.simpleobjectstorage.fileupload;


import lombok.RequiredArgsConstructor;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teparak.me.backendtech.simpleobjectstorage.bucket.BucketDao;
import teparak.me.backendtech.simpleobjectstorage.exceptions.BucketNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.MD5MismatchException;
import teparak.me.backendtech.simpleobjectstorage.exceptions.ObjectNotFoundException;
import teparak.me.backendtech.simpleobjectstorage.utils.EmptyObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/{bucketName}/{objectName}")
@RequiredArgsConstructor(onConstructor_={ @Autowired })
public class FileUploadController {

    private final FileUploadService fileUploadService;

    @RequestMapping(method = RequestMethod.POST, params = "create")
    public ResponseEntity<?> createTicket(@PathVariable String bucketName, @PathVariable String objectName){

        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        try {
            fileUploadService.createTicket(bucketDao, objectDao);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.PUT, params = "partNumber")
    public ResponseEntity<?> putUpload(@PathVariable String bucketName, @PathVariable String objectName, @RequestParam Long partNumber, @RequestHeader("Content-Length") Long contentLength, @RequestHeader(value = "Content-MD5", required = false) String md5, HttpServletRequest request) throws IOException {
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);

        // TODO: validate part number range


        Map<String, String> out = new HashMap<>();
        out.put("md5", md5);
        out.put("length", String.valueOf(contentLength));
        out.put("partNumber", String.valueOf(partNumber));

        if (partNumber == null ||  partNumber <= 0 || partNumber > 10000){
            out.put("error", "InvalidPartNumber");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        }
        if ( md5 == null || md5.trim().isEmpty()){
            out.put("error", "MD5Mismatched");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        }


        try {
            fileUploadService.uploadPartToBucket(bucketDao, objectDao, partNumber,request.getInputStream(), contentLength,  md5);
        }catch (MD5MismatchException e){
            out.put("error", "MD5Mismatched");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        } catch (Exception e){
            out.put("error", "");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.DELETE, params = "partNumber")
    public ResponseEntity<?> deleteIncompletePart(@PathVariable String bucketName, @PathVariable String objectName, @RequestParam Long partNumber){
        // TODO: Implement me
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);
        try {
            fileUploadService.deleteIncompletePart(bucketDao, objectDao, partNumber);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.POST, params = "complete")
    public ResponseEntity<?> completeMultipartUpload(@PathVariable String bucketName, @PathVariable String objectName){
        // TODO: pullout Content-MD5 and validate
        BucketDao bucketDao = BucketDao.of(bucketName);
        ObjectDao objectDao = ObjectDao.of(objectName);

        Map<String, String> out = new HashMap<>();
        try {
            ObjectDao dao = fileUploadService.completeTicket(bucketDao, objectDao);
            out.put("eTag", dao.getETag());
            out.put("length", String.valueOf(dao.getContentLength()));
        } catch (BucketNotFoundException e){
            out.put("error", "InvalidBucket");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        } catch (ObjectNotFoundException e) {
            out.put("error", "InvalidObjectName");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(out);
        }
        return ResponseEntity.ok().build();
    }
}
